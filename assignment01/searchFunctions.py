import util
from util import Stack , Queue , PriorityQueue

from game import Directions

UNREACHABLE_GOAL_STATE = [Directions.STOP]


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    s = Directions.SOUTH
    w = Directions.WEST
    return [s, s, w, s, w, w, s, w]


def right_hand_maze_search(problem):
    """
    Q1: Search using right hand rule

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:
    """
    cur = problem.getStartState()
    actions = []
    real_directions = Directions.RIGHT
    direction = "West"

    while problem.isGoalState(cur) == False:
        next_states = problem.getNextStates(cur)
        if direction == 'North' :
            real_directions['East'] = 'East'
            real_directions['North'] = 'North'
            real_directions['West'] = 'West'
            real_directions['South'] = 'South'
        if direction == 'East':
            real_directions['East'] = 'South'
            real_directions['North'] = 'East'
            real_directions['West'] = 'North'
            real_directions['South'] = 'West'
        if direction == 'South':
            real_directions['East'] = 'West'
            real_directions['North'] = 'South'
            real_directions['West'] = 'East'
            real_directions['South'] = 'North'
        if direction == 'West':
            real_directions['East'] = 'North'
            real_directions['North'] = 'West'
            real_directions['West'] = 'South'
            real_directions['South'] = 'East'
        moved = False
        for i in next_states :
            if i[1] == real_directions['East'] and moved == False:
                cur = i[0]
                moved = True
                actions.append(real_directions['East'])
                direction = real_directions['East']

        for i in next_states :
            if i[1] == real_directions['North'] and moved == False:
                cur = i[0]
                moved = True
                actions.append(real_directions['North'])
                direction = real_directions['North']


        for i in next_states :
            if i[1] == real_directions['West'] and moved == False:
                cur = i[0]
                moved = True
                actions.append(real_directions['West'])
                direction = real_directions['West']

        for i in next_states :
            if i[1] == real_directions['South'] and moved == False:
                cur = i[0]
                moved = True
                actions.append(real_directions['South'])
                direction = real_directions['South']




    return actions


def dfs(problem):
    cur = problem.getStartState()
    mark = {}
    par = {}
    par[cur] = cur
    path = {}
    stack = Stack()
    stack.push(cur)
    actions = []
    while (problem.isGoalState(cur) == False ):
        cur = stack.pop()
        mark[cur] = 1
        next_state = problem.getNextStates(cur)
        for i in next_state :
            if i[0] not in mark and i[2] != 100000 :
                stack.push(i[0])
                par[i[0]] = cur
                path[i[0]] = i[1]



    while par[cur] != cur :
        actions.append(path[cur])
        cur = par[cur]

    actions.reverse()
    return actions



def bfs(problem):
    cur = problem.getStartState()
    mark = {}
    par = {}
    par[cur] = cur
    path = {}
    queue = Queue()
    queue.push(cur)
    actions = []
    while (problem.isGoalState(cur) == False):
        cur = queue.pop()
        mark[cur] = 1
        next_state = problem.getNextStates(cur)
        for i in next_state:
            if i[0] not in mark:
                mark[i[0]] = 1
                queue.push(i[0])
                par[i[0]] = cur
                path[i[0]] = i[1]

    while par[cur] != cur:
        actions.append(path[cur])
        cur = par[cur]

    actions.reverse()
    return actions


def ucs(problem):
    """
    Q6: Search the node of least total cost first.
    """
    cur = problem.getStartState()
    mark = {}
    par = {}
    par[cur] = cur
    path = {}
    cost = {}
    cost[cur] = 0
    queue = PriorityQueue()
    queue.push(cur , 0)
    actions = []
    while (problem.isGoalState(cur) == False):
        cur = queue.pop()
        mark[cur] = 1
        next_state = problem.getNextStates(cur)
        for i in next_state:
            if i[0] not in mark:
                mark[i[0]] = 1
                queue.update(i[0] , cost[cur] + i[2])
                cost[i[0]] = cost[cur] + i[2]
                print cost[i[0]]
                par[i[0]] = cur
                path[i[0]] = i[1]


    while par[cur] != cur:
        actions.append(path[cur])
        cur = par[cur]

    actions.reverse()
    return actions
